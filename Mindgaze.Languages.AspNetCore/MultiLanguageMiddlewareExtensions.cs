﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.Languages.AspNetCore
{
    public static class MultiLanguageMiddlewareExtensions
    {
        public static IApplicationBuilder UseRequestMultiLanguageLocalization(this IApplicationBuilder app)
        {
            var multiLanguageService = app.ApplicationServices.GetService(typeof(MultiLanguageService)) as MultiLanguageService;
            
            app.UseRequestLocalization(multiLanguageService.LocalizationOptions);
            //app.UseMiddleware<MultiLanguageMiddleware>(new MultiLanguageCultureProvider(multiLanguageService));

            return app;
        }
    }
}
