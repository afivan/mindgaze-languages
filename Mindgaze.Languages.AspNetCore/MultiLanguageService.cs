﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Mindgaze.Utilities.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.Languages.AspNetCore
{
    public class MultiLanguageService
    {
        private readonly string JsonStrings;
        private readonly string JsonConfig;

        private readonly MultiLanguageWorkshop DeterminantWorkshop;

        internal MultiLanguageService(string jsonStrings, string jsonConfig)
        {
            JsonStrings = jsonStrings;
            JsonConfig = jsonConfig;
            DeterminantWorkshop = RetrieveLanguageSupplier();
        }

        private async Task<MultiLanguageWorkshop> RetrieveLanguageSupplierAsync()
        {
            return await MultiLanguageWorkshopSlabPool.CreateLanguageSupplierAsync(JsonStrings, JsonConfig);
        }

        private MultiLanguageWorkshop RetrieveLanguageSupplier()
        {
            return AsyncHelpers.RunSync<MultiLanguageWorkshop>(async () =>
            {
                return await RetrieveLanguageSupplierAsync();
            });
        }


        public Func<IServiceProvider, MultiLanguageWorkshop> ImplementationFactory
        {
            get
            {
                return (provider) =>
                {
                    
                    var workshop = RetrieveLanguageSupplier();
                    try
                    {
                        workshop.CurrentLanguage = CultureInfo.CurrentCulture.Name;
                    }
                    catch { }
                    return workshop;
                };
            }
        }
            
        internal IEnumerable<string> SupportedCultures
        {
            get
            {
                return DeterminantWorkshop.Languages;
            }
        }

        private CultureInfo DefaultCulture
        {
            get
            {
                return new CultureInfo(DeterminantWorkshop.Languages.First());
            }
        }

        public RequestLocalizationOptions LocalizationOptions
        {
            get
            {
                var requestLocalizationOptions = new RequestLocalizationOptions();
                var supportedCultures = new List<CultureInfo>();

                foreach(var cultureName in DeterminantWorkshop.Languages)
                {
                    supportedCultures.Add(new CultureInfo(cultureName));
                }

                requestLocalizationOptions.SupportedCultures = supportedCultures;
                requestLocalizationOptions.SupportedUICultures = supportedCultures;

                requestLocalizationOptions.RequestCultureProviders = new List<IRequestCultureProvider>()
                {
                    new MultiLanguageCultureProvider(this)
                };

                return requestLocalizationOptions;
            }
        }

        public RequestCulture DefaultRequestCulture
        {
            get
            {
                return new RequestCulture(DefaultCulture);
            }
        }
    }
}
