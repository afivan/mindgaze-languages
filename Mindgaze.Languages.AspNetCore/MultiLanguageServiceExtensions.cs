﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.Languages.AspNetCore
{
    public static class MultiLanguageServiceExtensions
    {
        public static IServiceCollection AddMultiLanguage(this IServiceCollection services, string jsonLanguagePath = "Strings/Strings.json", string jsonConfigPath = "Strings/Strings.config.json")
        {
            MultiLanguageService languageService = new MultiLanguageService(jsonLanguagePath, jsonConfigPath);
            
            services.AddScoped(languageService.ImplementationFactory);
            services.AddSingleton((provider) => { return languageService; });

            return services;
        }
        
        public static MultiLanguageService GetProvidedMultiLanguageService(this IServiceProvider provider)
        {
            return provider.GetService(typeof(MultiLanguageService)) as MultiLanguageService;
        }
    }
}
