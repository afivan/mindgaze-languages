﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Mindgaze.Languages.AspNetCore.TagHelpers
{
    public class LangTagHelper : TagHelper
    {
        private readonly MultiLanguageWorkshop Workshop;

        public LangTagHelper(MultiLanguageWorkshop workshop)
        {
            Workshop = workshop;
        }

        private dynamic LanguageSupplier
        {
            get
            {
                return Workshop;
            }
        }

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            var content = await output.GetChildContentAsync();
            var key = content.GetContent();
            if (FormatParams != null)
            {
                output.Content.SetContent(String.Format(Workshop[key], FormatParams));
            }
            else
            {
                output.Content.SetContent(Workshop[key]);
            }
        }

        public object[] FormatParams
        {
            get;set;
        }
    }
}
