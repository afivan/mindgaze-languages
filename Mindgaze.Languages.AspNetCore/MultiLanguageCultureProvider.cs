﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Primitives;

namespace Mindgaze.Languages.AspNetCore
{
    internal class MultiLanguageCultureProvider : IRequestCultureProvider
    {

        //
        // Summary:
        //     An ordered list of providers used to determine a request's culture information.
        //     The first provider that returns a non-null result for a given request will be
        //     used. Defaults to the following: Microsoft.AspNet.Localization.QueryStringRequestCultureProvider
        //     Microsoft.AspNet.Localization.CookieRequestCultureProvider Microsoft.AspNet.Localization.AcceptLanguageHeaderRequestCultureProvider
        private readonly IList<IRequestCultureProvider> RequestCultureProviders;
        private readonly MultiLanguageService LanguageService;

        public MultiLanguageCultureProvider(MultiLanguageService service)
        {
            var providers = new List<IRequestCultureProvider>();

            providers.Add(new QueryStringRequestCultureProvider());
            providers.Add(new CookieRequestCultureProvider());
            providers.Add(new AcceptLanguageHeaderRequestCultureProvider());

            RequestCultureProviders = providers;
            
            LanguageService = service;
        }

        public async Task<ProviderCultureResult> DetermineProviderCultureResult(HttpContext httpContext)
        {
            ProviderCultureResult cultureResult = null;

            foreach(var provider in RequestCultureProviders)
            {
                cultureResult = await provider.DetermineProviderCultureResult(httpContext);

                if(cultureResult!=null)
                {
                    break;
                }
            }

            var supportedCultures = LanguageService.SupportedCultures.Select(c => new StringSegment(c));

            if (cultureResult == null)
            {
                cultureResult = new ProviderCultureResult(supportedCultures.ToList());
            }
            else
            {
                var langUnion = cultureResult.Cultures.Intersect(supportedCultures);
                if (langUnion.Any())
                {
                    cultureResult = new ProviderCultureResult(langUnion.ToList());
                }
                else
                {
                    cultureResult = new ProviderCultureResult(supportedCultures.ToList());
                }
            }

            return cultureResult;
        }
    }
}
