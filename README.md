# Mindgaze.Languages

## Introduction

Create multilanguage applications for dotnet projects (ASP.NET Core, Windows Store, Windows Universal 10) with keys/values defined in **JSON** files. Provides easy validation with **JSON Schema** and easy integration of new language specifications. 

### Features

0. No need to manualy parse and validate the JSON files. You just need 2 files to get started: the language and the config.
0. Internal JSON parsing with JSchema ensures that all keys have the values specified for all the supported languages.
0. Service feature for ASP.NET Core that reads the supported client culture (from headers, cookie or query string) and uses the most appropiate language defined.
0. Tag helpers for ASP.NET Core ensures code stays clean and elegant.
0. Internally raises property changed notifications. Very handy in XAML based applications where databinding is heavily used. 
0. By using this library, you can create Windows Store apps that reactively switch the language, without the need of restart. This is particularly useful in **kiosk apps**.
0. From the main language file, you can include other files as well. The library takes care of loading and validating them
0. If your strings are more complex like a paragraph or even a whole page, you can store them in separate text files which can then be loaded by the tool.

## Installation

The easiest way to install it is through Nuget.

* Enter the `Install-Package Mindgaze.Languages` in the Package Management Console
* You're now ready to go! Please go to the [wiki](https://github.com/andreiflaviusivan/mindgaze.languages/wiki) for tutorials!
