﻿using Mindgaze.Utilities.Extensions;
using Mindgaze.Utilities.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Mindgaze.Languages
{
    public partial class MultiLanguageWorkshop
    {
        private const string ConfigSchemaFile = "Config.schema.json";
        private const string ConfigStringsSchemaFile = "Config.Strings.schema.json";

        private readonly JToken ConfigLangDefinitionToken = JToken.FromObject(new { type = "string" });

        private readonly Assembly CurrentLoadedAssembly = typeof(MultiLanguageWorkshop).GetTypeInfo().Assembly;

        private void ValidateAgainstSchema(JObject jObject, JSchema jSchema)
        {
            IList<String> jsonValidationErrors = new List<string>();

            if (!jObject.IsValid(jSchema, out jsonValidationErrors))
            {
                StringBuilder builder = new StringBuilder("Errors while parsing the language JSON file: \n");

                foreach (var error in jsonValidationErrors)
                {
                    builder.AppendLine(String.Format("- {0}", error));
                }

                throw new ArgumentException("JSON Schema parsing failed: " + builder.ToString());
            }
        }

        private async Task<String> LoadTextFileAsync(String filePath)
        {
            string result = string.Empty;
            using (var textReader = new StreamReader(File.OpenRead(filePath)))
            {
                result = await textReader.ReadToEndAsync();
            }
            return result;
        }

        private async Task<String> LoadTextFileAsync(Assembly assembly, String fileResourceName)
        {
            string result = string.Empty;
            using (var textReader = new StreamReader(assembly.GetManifestResourceStream(fileResourceName)))
            {
                result = await textReader.ReadToEndAsync();
            }
            return result;
        }

        private async Task<JSchema> ConstructValidatingSchema(String jsonConfigPath)
        {
            return await ConstructValidatingSchema(await JsonHelpers.ReadJObjectAsync(jsonConfigPath), await JsonHelpers.ReadJSchemaAsync(CurrentLoadedAssembly.LoadResource(ConfigSchemaFile)));
        }

        private async Task<JSchema> ConstructValidatingSchema(JObject jsonConfig)
        {
            return await ConstructValidatingSchema(jsonConfig, await JsonHelpers.ReadJSchemaAsync(CurrentLoadedAssembly.LoadResource(ConfigSchemaFile)));
        }

        private async Task<JSchema> ConstructValidatingSchema(JObject languageJsonConfig, JSchema languageJsonConfigSchema)
        {
            ValidateAgainstSchema(languageJsonConfig, languageJsonConfigSchema);

            // Construct validating schema
            JObject languageJsonValidatingConstructor = await JsonHelpers.ReadJObjectAsync(CurrentLoadedAssembly.LoadResource(ConfigStringsSchemaFile));

            var defObject = languageJsonValidatingConstructor["defLanguageObject"];
            var defObjectProperties = defObject["properties"].Value<JObject>();
            defObject.Value<JObject>().Add("required", new JArray());
            var defObjectRequired = defObject["required"].Value<JArray>();

            foreach (var language in languageJsonConfig["languages"].Value<JArray>())
            {
                var langName = language.Value<String>("name");

                // Add to validating schema properties
                defObjectProperties.Add(langName, ConfigLangDefinitionToken);
                // Add to validating schema requiredProperties
                defObjectRequired.Add(langName);
            }

            return await JsonHelpers.JObjectToJSchema(languageJsonValidatingConstructor);
        }

        private string JustResourcePath(string resource)
        {
            var split = resource.Split('.');

            return split
                .Take(split.Length - 2)
                .Aggregate((v1, v2) => $"{v1}.{v2}")
                ;
        }

        private async Task<JObject> LoadJsonObjectWithValidation(String jsonPath, JSchema jsonSchema)
        {
            JObject languageJsonObject = await JsonHelpers.ReadJObjectAsync(jsonPath);
            
            ValidateAgainstSchema(languageJsonObject, jsonSchema);

            return languageJsonObject;
        }

        private async Task<JObject> LoadJsonObjectWithValidation(Assembly assembly, String jsonResource, JSchema jsonSchema)
        {
            JObject languageJsonObject = await JsonHelpers.ReadJObjectAsync(assembly.GetManifestResourceStream(jsonResource));

            ValidateAgainstSchema(languageJsonObject, jsonSchema);

            return languageJsonObject;
        }

        private async Task<JObject> LoadJsonObjectWithExternalStrings(String jsonPath, JSchema jsonSchema)
        {
            var json = await LoadJsonObjectWithValidation(jsonPath, jsonSchema);

            // In future, check if this can be written with JsonPath completely
            var tokensToBeLoaded = json.SelectTokens("$.*.*").Where(t => t.Value<String>().StartsWith(LoadExternalFilesKey));

            foreach (var token in tokensToBeLoaded)
            {
                var pathString = token.Value<String>().Replace(LoadExternalFilesKey, String.Empty);
                
                var includedJsonFilePath = $"{PathHelper.CombinePathRelativeToFile(jsonPath, pathString)}";
                var text = await LoadTextFileAsync(includedJsonFilePath);
                token.Replace(new JValue(text));
            }
            
            return json;
        }

        private async Task<JObject> LoadJsonObjectWithExternalStrings(Assembly assembly, String jsonResource, JSchema jsonSchema)
        {
            var json = await LoadJsonObjectWithValidation(assembly, jsonResource, jsonSchema);

            // In future, check if this can be written with JsonPath completely
            var tokensToBeLoaded = json.SelectTokens("$.*.*").Where(t => t.Value<String>().StartsWith(LoadExternalFilesKey));

            foreach (var token in tokensToBeLoaded)
            {
                var resourceNameString = token.Value<String>().Replace(LoadExternalFilesKey, String.Empty);

                var includedJsonResourceName = $"{JustResourcePath(jsonResource)}.{resourceNameString}";
                var text = await LoadTextFileAsync(assembly, includedJsonResourceName);
                token.Replace(new JValue(text));
            }

            return json;
        }

        private async Task LoadIncludedJsonFiles(JObject json, String jsonPath, JSchema jsonSchema)
        {
            // Included JSONs
            if (json[IncludeJsonFilesKey] != null && json[IncludeJsonFilesKey].Value<JArray>() != null)
            {
                JToken[] tokenArray = new JToken[json[IncludeJsonFilesKey].Value<JArray>().Count];
                json[IncludeJsonFilesKey].Value<JArray>().CopyTo(tokenArray, 0);

                foreach (var token in tokenArray)
                {
                    var pathString = token.Value<String>();

                    var includedJsonFilePath = $"{PathHelper.CombinePathRelativeToFile(jsonPath, pathString)}";
                    var includedJson = await LoadJsonObjectWithExternalStrings(includedJsonFilePath, jsonSchema);

                    await LoadIncludedJsonFiles(includedJson, includedJsonFilePath, jsonSchema);

                    json.Merge(includedJson);
                }
            }
        }

        private async Task LoadIncludedJsonFiles(JObject json, Assembly assembly, String jsonResourceName, JSchema jsonSchema)
        {
            // Included JSONs
            if (json[IncludeJsonFilesKey] != null && json[IncludeJsonFilesKey].Value<JArray>() != null)
            {
                JToken[] tokenArray = new JToken[json[IncludeJsonFilesKey].Value<JArray>().Count];
                json[IncludeJsonFilesKey].Value<JArray>().CopyTo(tokenArray, 0);

                foreach (var token in tokenArray)
                {
                    var resourceString = token.Value<String>();

                    var includedJsonResourceName = $"{JustResourcePath(jsonResourceName)}.{resourceString}";
                    
                    var includedJson = await LoadJsonObjectWithExternalStrings(assembly, includedJsonResourceName, jsonSchema);
                    await LoadIncludedJsonFiles(includedJson, assembly, includedJsonResourceName, jsonSchema);

                    json.Merge(includedJson);
                }
            }
        }
    }
}
