﻿using Mindgaze.Utilities.Helpers;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Mindgaze.Languages
{
    public sealed partial class MultiLanguageWorkshop : DynamicObject, INotifyPropertyChanged
    {
        #region Static members
        
        public const String AllItemsIndexerName = "Item";
        public const string LanguageDescriptionKey = "#LanguageDescription";
        public const string IncludeJsonFilesKey = "#Include";
        public const string LoadExternalFilesKey = "#Load:";

        #endregion

        #region Readonly members

        private readonly Dictionary<string, MultiLanguageDescriptor> SpecificLanguages = new Dictionary<string, MultiLanguageDescriptor>();

        #endregion

        #region Private members

        #region Variables

        private String currentLanguage;
        private Uri jsonUri, jsonSchemaUri;

        private string jsonUriString, jsonSchemaUriString;
        
        private readonly HashSet<String> availableLanguages = new HashSet<string>();

        private string defaultEmptyString = "<EMPTY>";

        private JObject jsonLanguageObject = new JObject();
        private JObject jsonLanguageConfig = new JObject();

        private MultiLanguageDescriptor descriptor;
        
        private readonly List<MultiLanguageWorkshop> AssociatedWorkshops = new List<MultiLanguageWorkshop>();

        #endregion

        #region Properties

        private JObject JsonLanguageObject
        {
            get
            {
                return jsonLanguageObject;
            }
            set
            {
                jsonLanguageObject = value;
            }
        }

        private JObject JsonLanguageConfig
        {
            get
            {
                return jsonLanguageConfig;
            }
            set
            {
                jsonLanguageConfig = value;
            }
        }

        #endregion

        #region Methods

        private void NotifyPropertyChangedForAll()
        {
            PropertyChanged(this, new PropertyChangedEventArgs(String.Empty));

            PropertyChanged(this, new PropertyChangedEventArgs(AllItemsIndexerName + "[]"));
        }

        private String RetrieveString(String key, String language)
        {
            if (String.IsNullOrEmpty(key))
            {
                return DefaultEmptyString;
            }

            if (!Languages.Contains(language))
            {
                throw new LanguageNotSupportedException(language);
            }

            try
            {
                return JsonLanguageObject[key][language].Value<String>();
            }
            catch
            {
                return DefaultEmptyString;
            }
        }

        private String RetrieveString(String key)
        {
            return RetrieveString(key, CurrentLanguage);
        }

        private void AfterLoadOperations(JObject jsonStrings, JObject jsonConfig)
        {
            lock (availableLanguages)
            {
                availableLanguages.Clear();
                // Fill language descriptions
                foreach (var langToken in jsonConfig["languages"].Value<JArray>())
                {
                    availableLanguages.Add(langToken["name"].Value<String>());
                }
            }

            JsonLanguageObject = jsonStrings;
            JsonLanguageConfig = jsonConfig;
            Descriptor = new MultiLanguageDescriptor(jsonConfig, this);
        }

        #endregion

        #endregion

        #region Constructors

        public MultiLanguageWorkshop()
        {

        }

        internal MultiLanguageWorkshop(JObject jsonStrings, JObject jsonConfig)
        {
            //JsonLanguageObject = jsonStrings;
            //JsonLanguageConfig = jsonConfig;

            AfterLoadOperations(jsonStrings, jsonConfig);
        }

        #endregion

        #region Protected members

        #region Properties


        #endregion

        #region Methods


        #region Event Handlers

        #endregion

        #endregion

        #endregion

        #region Public members

        #region Properties

        public String CurrentLanguage
        {
            get
            {

                if (String.IsNullOrEmpty(currentLanguage) && availableLanguages.Count > 0)
                {
                    currentLanguage = availableLanguages.First();
                }

                return currentLanguage;
            }
            set
            {


                if (!availableLanguages.Contains(value))
                {
                    throw new LanguageNotSupportedException(value);
                }

                currentLanguage = value;

                // Same for associated workshops
                foreach (var wShop in AssociatedWorkshops)
                {
                    wShop.CurrentLanguage = currentLanguage;
                }

                PropertyChanged(this, new PropertyChangedEventArgs("CurrentLanguage"));

                NotifyPropertyChangedForAll();
            }
        }

        public IEnumerable<String> Languages
        {
            get
            {
                return availableLanguages;
            }
        }

        public IEnumerable<dynamic> LanguagesDescriptors
        {
            get
            {
                return Languages.Select(lang => GetLanguageDescriptor(lang));
            }
        }

        [IndexerName(AllItemsIndexerName)]
        public string this[string key]
        {
            get
            {
                return RetrieveString(key);

            }
        }

        public Uri JsonUri
        {
            get
            {
                return jsonUri;
            }
            private set
            {
                jsonUri = value;

                //if (JsonUri != null && JsonSchemaUri != null)
                //{
                //	InitializeLanguages();
                //}

            }
        }

        public Uri JsonSchemaUri
        {
            get
            {
                return jsonSchemaUri;
            }
            private set
            {
                jsonSchemaUri = value;

                //if (JsonUri != null && JsonSchemaUri != null)
                //{
                //	InitializeLanguages();
                //}
            }
        }

        #region Json Uri Strings


        // TODO Implement these with a special class called UriSource/JsonSource
        public String JsonUriString
        {
            get
            {
                return jsonUriString;
            }
            set
            {


                if (!String.IsNullOrEmpty(value))
                {
                    jsonUriString = value;

                    JsonUri = new Uri(JsonUriString, UriKind.RelativeOrAbsolute);
                }

            }
        }

        public String JsonSchemaUriString
        {
            get
            {
                return jsonSchemaUriString;
            }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    jsonSchemaUriString = value;

                    JsonSchemaUri = new Uri(JsonSchemaUriString, UriKind.RelativeOrAbsolute);
                }
            }
        }

        public String DefaultEmptyString
        {
            get
            {
                return defaultEmptyString;
            }
            set
            {
                defaultEmptyString = value;
                // TODO Add a method that has a [CallerName] attribute like in the BindableBase class
                PropertyChanged(this, new PropertyChangedEventArgs(nameof(DefaultEmptyString)));
            }
        }

        public dynamic Descriptor
        {
            get
            {
                return descriptor;
            }
            private set
            {
                descriptor = value;
                PropertyChanged(this, new PropertyChangedEventArgs(nameof(Descriptor)));
            }
        }

        #endregion


        #endregion

        #region Methods

        public String GetForLanguage(String key, String language)
        {
            return RetrieveString(key, language);
        }

        public dynamic GetLanguageDescriptor(string language)
        {
            if (Languages.Contains(language))
            {
                if (!SpecificLanguages.ContainsKey(language))
                {
                    SpecificLanguages.Add(language, new MultiLanguageDescriptor(JsonLanguageConfig, language));
                }

                return SpecificLanguages[language];
            }

            throw new LanguageNotSupportedException(language);
        }

        public void ClearAssociations()
        {
            AssociatedWorkshops.Clear();
        }

        public void AssociateWith(MultiLanguageWorkshop workshop)
        {
            if (!AssociatedWorkshops.Contains(workshop))
            {
                if (Enumerable.SequenceEqual(Languages, workshop.Languages))
                {
                    AssociatedWorkshops.Add(workshop);
                }
                else
                {
                    throw new ArgumentException("You're trying to associate with a workshop that supports different languages!");
                }
            }
        }

        [Obsolete("Use LoadLanguagesAsync instead")]
        public async Task LoadLanguages(String jsonLanguageFilePath, String jsonConfigFilePath)
        {
            if(String.IsNullOrEmpty(jsonLanguageFilePath))
            {
                throw new ArgumentNullException(nameof(jsonLanguageFilePath));
            }

            if (String.IsNullOrEmpty(jsonConfigFilePath))
            {
                throw new ArgumentNullException(nameof(jsonConfigFilePath));
            }

            if (!File.Exists(jsonLanguageFilePath))
            {
                throw new FileNotFoundException(nameof(jsonLanguageFilePath), jsonLanguageFilePath);
            }

            if (!File.Exists(jsonConfigFilePath))
            {
                throw new FileNotFoundException(nameof(jsonConfigFilePath), jsonConfigFilePath);
            }

            var jsonConfig = await JsonHelpers.ReadJObjectAsync(jsonConfigFilePath);
            var validatingSchema = await ConstructValidatingSchema(jsonConfig);
            var json = await LoadJsonObjectWithExternalStrings(jsonLanguageFilePath, validatingSchema);
            await LoadIncludedJsonFiles(json, jsonLanguageFilePath, validatingSchema);

            AfterLoadOperations(json, jsonConfig);
        }

        public async Task LoadLanguagesAsync(String jsonLanguageFilePath, String jsonConfigFilePath)
        {
            if (String.IsNullOrEmpty(jsonLanguageFilePath))
            {
                throw new ArgumentNullException(nameof(jsonLanguageFilePath));
            }

            if (String.IsNullOrEmpty(jsonConfigFilePath))
            {
                throw new ArgumentNullException(nameof(jsonConfigFilePath));
            }

            if (!File.Exists(jsonLanguageFilePath))
            {
                throw new FileNotFoundException(nameof(jsonLanguageFilePath), jsonLanguageFilePath);
            }

            if (!File.Exists(jsonConfigFilePath))
            {
                throw new FileNotFoundException(nameof(jsonConfigFilePath), jsonConfigFilePath);
            }

            var jsonConfig = await JsonHelpers.ReadJObjectAsync(jsonConfigFilePath);
            var validatingSchema = await ConstructValidatingSchema(jsonConfig);
            var json = await LoadJsonObjectWithExternalStrings(jsonLanguageFilePath, validatingSchema);
            await LoadIncludedJsonFiles(json, jsonLanguageFilePath, validatingSchema);

            AfterLoadOperations(json, jsonConfig);
        }

        public async Task LoadLanguagesAsync(Assembly assembly, String jsonLanguageResourceName, String jsonConfigResourceName)
        {
            if (String.IsNullOrEmpty(jsonLanguageResourceName))
            {
                throw new ArgumentNullException(nameof(jsonLanguageResourceName));
            }

            if (String.IsNullOrEmpty(jsonConfigResourceName))
            {
                throw new ArgumentNullException(nameof(jsonConfigResourceName));
            }

            var assemblyJsonResourceName = $"{assembly.GetName().Name}.{jsonLanguageResourceName}";
            var assemblyJsonConfigResourceName = $"{assembly.GetName().Name}.{jsonConfigResourceName}";

            if (assembly.GetManifestResourceInfo(assemblyJsonResourceName) == null)
            {
                throw new FileNotFoundException(nameof(jsonLanguageResourceName), jsonLanguageResourceName);
            }

            if (assembly.GetManifestResourceInfo(assemblyJsonConfigResourceName) == null)
            {                
                throw new FileNotFoundException(nameof(jsonConfigResourceName), jsonConfigResourceName);
            }

            var jsonConfig = await JsonHelpers.ReadJObjectAsync(assembly.GetManifestResourceStream(assemblyJsonConfigResourceName));
            var validatingSchema = await ConstructValidatingSchema(jsonConfig);
            var json = await LoadJsonObjectWithExternalStrings(assembly, assemblyJsonResourceName, validatingSchema);
            await LoadIncludedJsonFiles(json, assembly, assemblyJsonResourceName, validatingSchema);

            AfterLoadOperations(json, jsonConfig);
        }

        #region Overriden members

        public override IEnumerable<string> GetDynamicMemberNames()
        {
            return JsonLanguageObject.Values<JProperty>().Select(p => p.Name);
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            var stringRes = RetrieveString(binder.Name);
            result = stringRes;

            return !String.Equals(stringRes, DefaultEmptyString);
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            return false;
        }

        #endregion

        #region Event Handlers

        #region INotifyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        #endregion

        #endregion

        #endregion

        #endregion
    }
}
