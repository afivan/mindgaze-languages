﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Mindgaze.Languages
{
    public class MultiLanguageDescriptor : DynamicObject, INotifyPropertyChanged
    {
        private readonly JObject JsonConfig;
        private readonly MultiLanguageWorkshop Workshop;
        private readonly string Language;

        private const String AllItemsIndexerName = "Item";

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        internal MultiLanguageDescriptor(JObject jsonConfig, MultiLanguageWorkshop workshop)
        {
            JsonConfig = jsonConfig;
            Workshop = workshop;

            Workshop.PropertyChanged += Workshop_PropertyChanged;
        }

        internal MultiLanguageDescriptor(JObject jsonConfig, string language)
        {
            JsonConfig = jsonConfig;
            Language = language;
        }

        private void Workshop_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            
        }

        private bool TryGetMemberImpl(String memberName, out string result)
        {
            var token = JsonConfig.SelectToken($"languages[?(@.name=='{Workshop?.CurrentLanguage ?? Language}')].extra.{memberName}");

            result = Workshop?.DefaultEmptyString;

            if (token == null)
            {
                return false;
            }
            
            result = token.Value<String>();

            return true;
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            string res = "";
            result = res;
            bool status = TryGetMemberImpl(binder.Name, out res);

            if(status)
            {
                result = res;
                return true;
            }
            return false;
        }

        [IndexerName("Item")]
        public string this[string key]
        {
            get
            {
                string value;
                TryGetMemberImpl(key, out value);
                return value;
            }
        }
    }
}
