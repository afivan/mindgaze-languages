﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mindgaze.Languages
{
    // Suggestion: this might be renamed to LanguageException or MultiLanguageException
    public class LanguageNotSupportedException : Exception
    {
        public readonly String Language;

        internal LanguageNotSupportedException(String language)
            : base(String.Format("Language {0} is not supported by the resource. This usually indicates that there is a bug in the implementation. See the Language property for more details.", language))
        {
            this.Language = language;
        }
    }
}
