﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.Languages
{
    public static class MultiLanguageWorkshopSlabPool
    {
        // TODO The files might change on disk
        private static readonly Dictionary<String, MultiLanguageWorkshop> WorkshopSlabPool =
            new Dictionary<string, MultiLanguageWorkshop>();

        private static async Task<MultiLanguageWorkshop> LoadLanguageSupplier(String jsonPath, String jsonConfigPath)
        {
            MultiLanguageWorkshop workshop = new MultiLanguageWorkshop();

            await workshop.LoadLanguagesAsync(jsonPath, jsonConfigPath);

            return workshop;
        }

        public static async Task<MultiLanguageWorkshop> CreateLanguageSupplierAsync(String jsonStringsPath, String jsonConfigPath)
        {
            var key = $"{jsonStringsPath}||{jsonConfigPath}";

            if (!WorkshopSlabPool.ContainsKey(key))
            {
                var workshop = await LoadLanguageSupplier(jsonStringsPath, jsonConfigPath);

                lock (WorkshopSlabPool)
                {
                    // DO the check again as another thread might already inserted the key
                    if (!WorkshopSlabPool.ContainsKey(key))
                    {
                        WorkshopSlabPool.Add(key, workshop);
                    }
                }

                return workshop;
            }

            var existingWorkshop = WorkshopSlabPool[key];
            JObject jsonStrings, jsonDescriptor;

            existingWorkshop.PassToCloneEngine(out jsonStrings, out jsonDescriptor);

            return new MultiLanguageWorkshop(jsonStrings, jsonDescriptor);

        }
    }
}
