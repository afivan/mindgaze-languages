﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mindgaze.Languages
{
    public partial class MultiLanguageWorkshop
    {
        internal void PassToCloneEngine(out JObject jsonObject, out JObject jsonLanguageConfig)
        {
            jsonObject = JsonLanguageObject;
            jsonLanguageConfig = JsonLanguageConfig;
        }
    }
}
