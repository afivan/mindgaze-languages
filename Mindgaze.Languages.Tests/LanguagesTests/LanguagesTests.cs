﻿using Mindgaze.Languages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Mindgaze.Languages.Tests.LanguagesTests
{
    public class LanguagesTests
    {
        private const string StringsFile = "LanguagesTestsData/Strings.json";
        private const string StringsConfigFile = "LanguagesTestsData/Strings.config.json";

        #region Initialize facts
        [Fact]
        public async Task CheckLanguagesCountInitializeFact()
        {
            // Arrange
            var workshop = new MultiLanguageWorkshop();

            // Act
            await workshop.LoadLanguagesAsync(StringsFile, StringsConfigFile);

            // Assert
            Assert.Equal(2, workshop.Languages.Count());
        }

        [Fact]
        public async Task CheckDefaultLanguageStringsDynamicFact()
        {
            // Arrange
            var workshop = new MultiLanguageWorkshop();
            dynamic dynWorkshop = workshop;

            // Act
            await workshop.LoadLanguagesAsync(StringsFile, StringsConfigFile);

            // Assert
            Assert.Equal("Piatra Craiului Museum", dynWorkshop.ApplicationTitle);
            Assert.Equal("Piatra Crdfaiului Museum", dynWorkshop.ApplicationAbout);
            Assert.Equal("My friend", dynWorkshop.GoodOldPal12);
        }

        [Fact]
        public async Task CheckDefaultLanguageStringsIndexerFact()
        {
            // Arrange
            var workshop = new MultiLanguageWorkshop();
            dynamic dynWorkshop = workshop;

            // Act
            await workshop.LoadLanguagesAsync(StringsFile, StringsConfigFile);

            // Assert
            Assert.Equal("Piatra Craiului Museum", dynWorkshop["ApplicationTitle"]);
            Assert.Equal("Piatra Crdfaiului Museum", dynWorkshop["ApplicationAbout"]);
            Assert.Equal("My friend", dynWorkshop["GoodOldPal12"]);
        }

        #endregion

        #region Basic checks (these tests may not respect AAA pattern)

        [Fact]
        public async Task CheckDefaultEmptyString()
        {
            // Arrange
            var workshop = new MultiLanguageWorkshop();

            // Act
            await workshop.LoadLanguagesAsync(StringsFile, StringsConfigFile);

            var defaultString = "defaultString";
            workshop.DefaultEmptyString = defaultString;

            // Acts and assertions
            Assert.Equal(defaultString, workshop.DefaultEmptyString);

            var checkLanguage = "ThisStringShould not be contained by the object";

            Assert.Equal(workshop.DefaultEmptyString, workshop[checkLanguage]);

            checkLanguage = "AnotherPropertyThatShouldNotBeContained";
            Assert.Equal(workshop.DefaultEmptyString, workshop[checkLanguage]);

            Assert.Throws<Microsoft.CSharp.RuntimeBinder.RuntimeBinderException>(() =>
            {
                Assert.Equal(workshop.DefaultEmptyString, ((dynamic)workshop).AnotherPropertyThatShouldNotBeContained);
            });
        }
        

        [Fact]
        public async Task CheckDynamicMemberNames()
        {
            // Arrange
            var workshop = new MultiLanguageWorkshop();

            // Act
            await workshop.LoadLanguagesAsync(StringsFile, StringsConfigFile);

            var enumeration = workshop.GetDynamicMemberNames();

            // Enumeration contains items
            Assert.True(enumeration.Count() > 0);
            // Enumeration has no null or empty string items
            Assert.True(enumeration.Where(key => String.IsNullOrEmpty(key)).Count() <= 0);
        }

        [Fact]
        public async Task CheckLanguagesProduceTheRightStrings()
        {
            // Arrange
            var workshop = new MultiLanguageWorkshop();

            // Act
            await workshop.LoadLanguagesAsync(StringsFile, StringsConfigFile);

            var enumeration = workshop.GetDynamicMemberNames();

            workshop.DefaultEmptyString = "An empty string that should not be inside the json file";

            // Acts and assertions
            Assert.True(workshop.Languages.Count() > 0);

            foreach (var lang in workshop.Languages)
            {
                workshop.CurrentLanguage = lang;

                foreach (var key in enumeration)
                {
                    Assert.NotNull(workshop[key]);
                    Assert.True(!String.Equals(workshop[key], workshop.DefaultEmptyString));

                }
            }
        }

        [Fact]
        public async Task CheckBadLanguageSwitchThrowsException()
        {
            // Arrange
            var workshop = new MultiLanguageWorkshop();

            // Act
            await workshop.LoadLanguagesAsync(StringsFile, StringsConfigFile);

            Assert.Throws<LanguageNotSupportedException>(() =>
            {
                workshop.CurrentLanguage = "martian-Mars";
            });
        }

        #endregion
    }
}
