﻿using Mindgaze.Languages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Mindgaze.Languages.Tests.LanguagesTests
{
    public class LanguagesSlabPoolTests
    {
        private const string StringsFile = "LanguagesTestsData/Strings.json";
        private const string StringsConfigFile = "LanguagesTestsData/Strings.config.json";

        [Fact]
        public async Task VerifyCreateMethodRetrievesDifferentInstances()
        {
            // Arrange
            var workshopMain = await MultiLanguageWorkshopSlabPool.CreateLanguageSupplierAsync(StringsFile, StringsConfigFile);
            var workshopArray = new MultiLanguageWorkshop[10];

            // Act
            for (int i = 0; i < workshopArray.Length; i++)
            {
                workshopArray[i] = await MultiLanguageWorkshopSlabPool.CreateLanguageSupplierAsync(StringsFile, StringsConfigFile);
            }

            // Assert
            for (int i = 1; i < workshopArray.Length; i++)
            {
                Assert.NotEqual(workshopArray[i], workshopArray[i - 1]);
            }
        }

        [Fact]
        public async Task VerifyStringsValues()
        {
            // Arrange
            var workshopMain = await MultiLanguageWorkshopSlabPool.CreateLanguageSupplierAsync(StringsFile, StringsConfigFile);
            var workshopArray = new MultiLanguageWorkshop[10];

            // Act
            for (int i = 0; i < workshopArray.Length; i++)
            {
                workshopArray[i] = await MultiLanguageWorkshopSlabPool.CreateLanguageSupplierAsync(StringsFile, StringsConfigFile);
            }

            // Assert
            for (int i = 1; i < workshopArray.Length; i++)
            {
                Assert.Equal(workshopArray[i]["ApplicationTitle"], workshopArray[i - 1]["ApplicationTitle"]);
                Assert.Equal(workshopArray[i]["ApplicationAbout"], workshopArray[i - 1]["ApplicationAbout"]);
            }
        }

        [Fact]
        public async Task VerifyStringsValuesWhenLanguageChanges()
        {
            // Arrange
            var workshopMain = await MultiLanguageWorkshopSlabPool.CreateLanguageSupplierAsync(StringsFile, StringsConfigFile);
            var workshopArray = new MultiLanguageWorkshop[10];

            // Act
            for (int i = 0; i < workshopArray.Length; i++)
            {
                workshopArray[i] = await MultiLanguageWorkshopSlabPool.CreateLanguageSupplierAsync(StringsFile, StringsConfigFile);
            }

            // Change language for the odd indexes
            for (int i = 1; i < workshopArray.Length; i += 2)
            {
                workshopArray[i].CurrentLanguage = "ro_RO";
            }

            // Assert
            
            // Check even indexes
            for (int i = 2; i < workshopArray.Length; i+= 2)
            {
                Assert.Equal(workshopArray[i]["ApplicationTitle"], workshopArray[i - 2]["ApplicationTitle"]);
                Assert.Equal(workshopArray[i]["ApplicationAbout"], workshopArray[i - 2]["ApplicationAbout"]);
            }

            // Check odd indexes
            for (int i = 3; i < workshopArray.Length; i += 2)
            {
                Assert.Equal(workshopArray[i]["ApplicationTitle"], workshopArray[i - 2]["ApplicationTitle"]);
                Assert.Equal(workshopArray[i]["ApplicationAbout"], workshopArray[i - 2]["ApplicationAbout"]);
            }

            // Check difference between odd and even
            for (int i = 1; i < workshopArray.Length; i++)
            {
                Assert.NotEqual(workshopArray[i]["ApplicationTitle"], workshopArray[i - 1]["ApplicationTitle"]);
                Assert.NotEqual(workshopArray[i]["ApplicationAbout"], workshopArray[i - 1]["ApplicationAbout"]);
            }
        }
    }
}
