﻿using Microsoft.CSharp.RuntimeBinder;
using Mindgaze.Languages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Mindgaze.Languages.Tests.LanguagesTests
{
    public class StringsConfigTests
    {
        private const string StringConfigFolder = "LanguagesTestsData/StringsConfig/";
        private readonly string StringsLanguageFile = $"{StringConfigFolder}Strings.json";

        [Fact]
        public async Task CheckLanguagesDescription()
        {
            // Arrange
            MultiLanguageWorkshop workshop = new MultiLanguageWorkshop();

            // Act
            await workshop.LoadLanguagesAsync(StringsLanguageFile, $"{StringConfigFolder}Strings.config.json");

            // Assert
            Assert.Equal("English", workshop.Descriptor.Description);

            // Act
            workshop.CurrentLanguage = "ro_RO";
            // Assert
            Assert.Equal("Română", workshop.Descriptor.Description);
        }

        [Fact]
        public async Task MissingDescriptions()
        {
            // Arrange
            MultiLanguageWorkshop workshop = new MultiLanguageWorkshop();

            // Act
            await workshop.LoadLanguagesAsync($"{StringConfigFolder}Strings-missing descriptions.json", $"{StringConfigFolder}Strings-missing descriptions.config.json");

            // Assert
            Assert.ThrowsAny<RuntimeBinderException>(() =>
            {
                Assert.Equal(workshop.DefaultEmptyString, workshop.Descriptor.Description);
            });

            // Act
            workshop.CurrentLanguage = "ro_RO";
            // Assert
            Assert.Equal("Română", workshop.Descriptor.Description);
        }

        [Fact]
        public async Task CheckLanguagesSeparateDescription()
        {
            // Arrange
            MultiLanguageWorkshop workshop = new MultiLanguageWorkshop();

            // Act
            await workshop.LoadLanguagesAsync(StringsLanguageFile, $"{StringConfigFolder}Strings.config.json");

            var englishDescriptor = workshop.GetLanguageDescriptor("en_US");
            var romanianDescriptor = workshop.GetLanguageDescriptor("ro_RO");

            // Assert
            Assert.Equal("English", englishDescriptor.Description);
            Assert.Equal("Română", romanianDescriptor.Description);
        }

        [Fact]
        public async Task CheckLanguagesDescriptors()
        {
            // Arrange
            MultiLanguageWorkshop workshop = new MultiLanguageWorkshop();

            // Act
            await workshop.LoadLanguagesAsync(StringsLanguageFile, $"{StringConfigFolder}Strings.config.json");

            var descriptors = workshop.LanguagesDescriptors;

            // Assert
            Assert.Equal("English", descriptors.ElementAt(0).Description);
            Assert.Equal("Română", descriptors.ElementAt(1).Description);
        }
    }
}
