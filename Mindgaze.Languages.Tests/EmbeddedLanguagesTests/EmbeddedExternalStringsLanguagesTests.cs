﻿using Mindgaze.Languages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Xunit;

namespace Mindgaze.Languages.Tests.EmbeddedLanguagesTests
{
    public class EmbeddedExternalStringsLanguagesTests
    {
        private const string StringsConfigFile = "EmbeddedLanguagesTestsData.Strings.config.json";

        private Assembly OurAssembly = typeof(EmbeddedExternalStringsLanguagesTests).Assembly;

        [Fact]
        public async Task CheckExternalsLoaded()
        {
            var workshop = new MultiLanguageWorkshop();

            await workshop.LoadLanguagesAsync(OurAssembly, "EmbeddedLanguagesTestsData.ExternalStrings.Strings.json", StringsConfigFile);

            // IncludeParent.json
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["ApplicationTitle"]);
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["ApplicationAbout"]);

            // en_US
            workshop.CurrentLanguage = "en_US";

            // External1
            Assert.Equal("This is external file number 1", workshop["Test_External1"]
                .Replace("\n", "")
                .Replace("\r", "")
                );

            // External2
            Assert.Equal("This is external file number 2", workshop["Test_External2"]
                .Replace("\n", "")
                .Replace("\r", "")
                );

            // External3
            Assert.Equal("<html>\r\n<body>\r\n    <h1>External 3</h1>\r\n\r\n    <p>This is HTML file number 3</p>\r\n</body>\r\n</html>"
                .Replace("\n", "")
                .Replace("\r", ""), 
                
                workshop["Test_External3"]
                .Replace("\n", "")
                .Replace("\r", "")
                );

            // ro_RO
            workshop.CurrentLanguage = "ro_RO";

            // External1
            Assert.Equal("This is inline/Acesta este inline", workshop["Test_External1"]);

            // External2
            Assert.Equal("Acesta este fișierul\r\nextern cu numărul 1"
                .Replace("\n", "")
                .Replace("\r", ""), 
                
                workshop["Test_External2"]
                .Replace("\n", "")
                .Replace("\r", "")
                );

            // External3
            Assert.Equal("<html>\r\n<body>\r\n    <h1>Extern 3</h1>\r\n\r\n    <p>Acesta este fișierul HTML extern cu numărul 3</p>\r\n</body>\r\n</html>"
                .Replace("\n", "")
                .Replace("\r", ""), 
                
                workshop["Test_External3"]
                .Replace("\n", "")
                .Replace("\r", "")
                );

        }

        [Fact]
        public async Task CheckBrokenExternalThrowsException()
        {
            var workshop = new MultiLanguageWorkshop();

            // 'System.IO.FileNotFoundException' 
            await Assert.ThrowsAsync<System.IO.FileNotFoundException>(async () =>
            {
                await workshop.LoadLanguagesAsync(OurAssembly, "EmbeddedLanguagesTestData.ExternalStrings.BrokenStrings.json", StringsConfigFile);
            });

        }
    }
}
