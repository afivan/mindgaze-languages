﻿using Mindgaze.Languages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Xunit;

namespace Mindgaze.Languages.Tests.EmbeddedLanguagesTests
{
    public class EmbeddedInclusionLanguagesTests
    {
        private const string StringsConfigFile = "EmbeddedLanguagesTestsData.Strings.config.json";
        private Assembly OurAssembly = typeof(EmbeddedInclusionLanguagesTests).Assembly;

        [Fact]
        public async Task CheckChildrenIncluded()
        {
            // Arrange
            var workshop = new MultiLanguageWorkshop();

            // Act
            await workshop.LoadLanguagesAsync(OurAssembly, "EmbeddedLanguagesTestsData.Inclusion.IncludeParent.json", StringsConfigFile);
            // Assert
            // IncludeParent.json
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["ApplicationTitle"]);
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["ApplicationAbout"]);

            // IncludeChild1.json
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["LabelHome"]);
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["LabelAbout"]);

            // IncludeChild1Child1.json
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["LabelScotch"]);
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["LabelJohnnie"]);

            // IncludeChild1Child2.json
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["LabelLight"]);
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["LabelSearch"]);

            // IncludeChild1Child3.json
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["LabelFord"]);
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["LabelSkoda"]);

            // IncludeChild2.json
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["ArticleTrips"]);
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["ArticleGossips"]);

            // IncludeChild2Child1.json
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["ArticleForest"]);
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["ArticleMountain"]);

            // IncludeChild2Child2.json
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["ArticleRefrigerator"]);
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["ArticleBottle"]);

            // IncludeRelative.json
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["RelativeHome"]);
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["RelativeAbout"]);

            // Relative/IncludeRelative.json
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["RelativePosition"]);
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["RelativeIntegration"]);
        }

        [Fact]
        public async Task CheckBrokenIncludeThrowsException()
        {
            var workshop = new MultiLanguageWorkshop();

            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                await workshop.LoadLanguagesAsync(OurAssembly, "EmbeddedLanguagesTestsData.Inclusion.IncludeBrokenParent.json", StringsConfigFile);
            });

        }

        [Fact]
        public async Task CheckAbsoluteUriInclusion()
        {
            // Arrange
            var workshop = new MultiLanguageWorkshop();

            // Act
            await workshop.LoadLanguagesAsync(OurAssembly, "EmbeddedLanguagesTestsData.Inclusion.IncludeParentAbsolute.json", StringsConfigFile);

            // Assert
            // IncludeChildAbsolute.json
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["ApplicationTitle"]);
            Assert.NotEqual(workshop.DefaultEmptyString, workshop["ApplicationAbout"]);

            Assert.Equal("Piatra Craiului Museum", workshop["ApplicationTitle"]);
            Assert.Equal("About Piatra Craiului Museum", workshop["ApplicationAbout"]);

            // Act
            workshop.CurrentLanguage = "ro_RO";

            Assert.Equal("Muzeul Piatra Craiului", workshop["ApplicationTitle"]);
            Assert.Equal("Despre Muzeul Piatra Craiului", workshop["ApplicationAbout"]);
        }
    }
}
